package ru.oz.learn12;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringConversions {
    private String stringToConvert;

    public void setStringToConvert(String stringToConvert) {
        this.stringToConvert = stringToConvert;
    }

    public String encloseEachWordInSquareBrackets() {
        if (stringToConvert != null) {
            String[] words = stringToConvert.split(" ");
            List<String> list = Arrays.stream(words)
                    .map(str -> "[" + str + "]")
                    .collect(Collectors.toList());
            return String.join(" ", list);
        } else {
            throw new RuntimeException("Строка равна null");
        }
    }

    public String reverseLine() {
        return StringUtils.reverse(stringToConvert);
    }
}
