package ru.oz.learn12;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * вам пришла новая задача - разграбить федеральное хранилище.
 * Вы должны получить все значения полей федерального хранилища, сохранить эту информацию,
 * установить федеральному хранилищу для всех полей нулевые или пустые значения,
 * и только после этого (!!!) создать собственное хранилище (с помощью приватного конструктора
 * класса Vault) используя награбленные данные в качестве параметров.
 * <p>
 * Т.е. по сути перенести данные из федерального хранилища в свое.
 */
public class Heist {
    public static void main(String[] args) {

        Vault federalVault = new Vault();

        int myDollars;
        int myEuros;
        double myTonsOfGold;
        String nowMyCodes;

        // получение значений всех полей через рефлексию
        try {
            Method methGetDollars = federalVault.getClass().getDeclaredMethod("getDollars");
            methGetDollars.setAccessible(true);
            myDollars = (int) methGetDollars.invoke(federalVault);

            Method methGetEuros = federalVault.getClass().getDeclaredMethod("getEuros");
            methGetEuros.setAccessible(true);
            myEuros = (int) methGetEuros.invoke(federalVault);

            Method methGetTonsOfGold = federalVault.getClass().getDeclaredMethod("getTonsOfGold");
            methGetTonsOfGold.setAccessible(true);
            myTonsOfGold = (double) methGetTonsOfGold.invoke(federalVault);

            Method methGetPentagonNukesCodes = federalVault.getClass().getDeclaredMethod("getPentagonNukesCodes");
            methGetPentagonNukesCodes.setAccessible(true);
            nowMyCodes = (String) methGetPentagonNukesCodes.invoke(federalVault);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }

        // заполнение всех полей пустыми/нулевыми значениями через рефлексию
        try {
            Method methSetDollars = federalVault.getClass().getDeclaredMethod("setDollars", int.class);
            methSetDollars.setAccessible(true);
            methSetDollars.invoke(federalVault, 0);

            Method methSetEuros = federalVault.getClass().getDeclaredMethod("setEuros", int.class);
            methSetEuros.setAccessible(true);
            methSetEuros.invoke(federalVault, 0);

            Method methSetTonsOfGold = federalVault.getClass().getDeclaredMethod("setTonsOfGold", double.class);
            methSetTonsOfGold.setAccessible(true);
            methSetTonsOfGold.invoke(federalVault, 0);

            Method methSetPentagonNukesCodes = federalVault.getClass().getDeclaredMethod("setPentagonNukesCodes", String.class);
            methSetPentagonNukesCodes.setAccessible(true);
            methSetPentagonNukesCodes.invoke(federalVault, "");
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }

        // создание собственного хранилища с помощью приватного конструктора
        try {
            Constructor<? extends Vault> constructor = federalVault.getClass().getDeclaredConstructor(int.class, int.class, double.class, String.class);
            constructor.setAccessible(true);
            Vault myConstructor = constructor.newInstance(myDollars, myEuros, myTonsOfGold, nowMyCodes);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException |
                 InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

}
