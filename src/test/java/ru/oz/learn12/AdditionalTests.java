package ru.oz.learn12;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AdditionalTests {
    StringConversions stringConversions = new StringConversions();
    String inputString;

    @BeforeClass
    public void setString() {
        inputString = "ffsfs bvcbc yrtyrt asdasdas";
        stringConversions.setStringToConvert(inputString);
    }

    @Test
    public void lengthsOfInputAndOutputStringsAreEqual() {
        String resultString = stringConversions.reverseLine();

        Assert.assertEquals(inputString.length(), resultString.length());
    }

    @Test
    public void lengthsOfInputAndOutputStringsAreNotEqual() {
        String resultString = stringConversions.encloseEachWordInSquareBrackets();

        Assert.assertNotEquals(inputString.length(), resultString.length());
    }

    @Test
    public void lastCharacterOfOutputStringIsEqualToFirstCharacterOfInputString() {
        String resultString = stringConversions.reverseLine();

        Assert.assertEquals(inputString.charAt(0), resultString.charAt(resultString.length() - 1));
    }

    @Test
    public void testForSquareBrackets() {
        String resultString = stringConversions.encloseEachWordInSquareBrackets();

        Assert.assertTrue(resultString.contains("["));
        Assert.assertTrue(resultString.contains("]"));
    }
}
