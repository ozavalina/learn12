package ru.oz.learn12;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class StringConversionTest {
    StringConversions stringConversions = new StringConversions();
    String inputString;

    @BeforeClass
    public void setString() {
        inputString = "ffsfs bvcbc yrtyrt asdasdas";
        stringConversions.setStringToConvert(inputString);
    }

    @Test
    public void testForEnclosingWordsInSquareBrackets() {
        String expectedString = "[ffsfs] [bvcbc] [yrtyrt] [asdasdas]";
        String resultString = stringConversions.encloseEachWordInSquareBrackets();

        Assert.assertEquals(resultString, expectedString);
    }

    @Test
    public void testReversedLine() {
        String expectedString = "sadsadsa trytry cbcvb sfsff";
        String resultString = stringConversions.reverseLine();

        Assert.assertEquals(resultString, expectedString);
    }
}
